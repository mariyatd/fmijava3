package bg.sofia.uni.fmi.mjt.jira;

import bg.sofia.uni.fmi.mjt.jira.enums.IssueResolution;
import bg.sofia.uni.fmi.mjt.jira.enums.WorkAction;
import bg.sofia.uni.fmi.mjt.jira.interfaces.Filter;
import bg.sofia.uni.fmi.mjt.jira.interfaces.Repository;
import bg.sofia.uni.fmi.mjt.jira.issues.Issue;

public class Jira implements Filter, Repository {

    private final static int MAX_ISSUES = 100;
    private int issueCounter = 0;
    private Issue[] issues;

    public Jira() {
        issues = new Issue[MAX_ISSUES];
    }

    public void addActionToIssue(Issue issue, WorkAction action, String actionDescription) {
        issue.addAction(action, actionDescription);
    }

    public void resolveIssue(Issue issue, IssueResolution resolution) {
        issue.resolve(resolution);
    }

    @Override
    public Issue find(String issueID) {
        if (issues != null) {
            for (int k = 0; k < issues.length; k++) {
                if (issues[k] != null && issueID.equals(issues[k].getIssueID())) {
                    return issues[k];
                }
            }
        }
        return null;
    }

    @Override
    public void addIssue(Issue issue) {
        if (find(issue.getIssueID()) != null){
            throw new RuntimeException("Issue " + issue.getIssueID() + " already exists.");
        }
        else if (issueCounter >= MAX_ISSUES) {
            throw new RuntimeException("Maximum issue count reached.");
        }
        issues[issueCounter] = issue;
        issueCounter++;
        return;
    }
}