package bg.sofia.uni.fmi.mjt.jira.issues;

import java.util.Objects;

public class Component {
    private String name;
    private String shortName;

    public Component(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Component)) {
            return false;
        }
        Component c = (Component) o;
        return CharSequence.compare(name, c.name) == 0 && CharSequence.compare(shortName, c.shortName) == 0;
    }

    public Object getShortName() {
        return this.shortName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, shortName);
    }
}
