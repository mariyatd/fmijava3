package bg.sofia.uni.fmi.mjt.jira.issues;

import bg.sofia.uni.fmi.mjt.jira.enums.IssuePriority;
import bg.sofia.uni.fmi.mjt.jira.enums.IssueResolution;
import bg.sofia.uni.fmi.mjt.jira.enums.IssueStatus;
import bg.sofia.uni.fmi.mjt.jira.enums.WorkAction;

import java.time.LocalDateTime;

public abstract class Issue {
    private static final int MAX_ACTIONS = 20;
    private static volatile int idCounter = 0;

    private String issueID;
    protected String description;
    private IssuePriority priority;
    private IssueResolution resolution;
    private IssueStatus status = IssueStatus.OPEN;
    protected WorkAction[] mandatoryActions;

    private Component component;
    private String[] actionLog;
    public WorkAction[] actions;
    private int actionCounter;

    private LocalDateTime timestamp;
    //I would rename this for consistency (timestamp)
    private LocalDateTime lastUpdated;

    public Issue() {
    }

    public Issue(IssuePriority priority, Component component, String description) {
        this.priority = priority;
        this.component = component;
        this.description = description;
        this.actionLog = new String[MAX_ACTIONS];
        this.actions = new WorkAction[MAX_ACTIONS];
        this.resolution = IssueResolution.UNRESOLVED;
        this.issueID = component.getShortName().toString() + "-" + idCounter;
        this.timestamp = LocalDateTime.now();
        this.lastUpdated = LocalDateTime.now();
        idCounter++;
    }

    public String getIssueID() {
        return issueID;
    }

    public String getDescription() {
        return description;
    }

    public IssuePriority getPriority() {
        return priority;
    }

    public IssueResolution getResolution() {
        return resolution;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public Component getComponent() {
        return component;
    }

    public LocalDateTime getCreatedOn() {
        return timestamp;
    }

    public LocalDateTime getLastModifiedOn() {
        return this.lastUpdated;
    }

    public String[] getActionLog() {
        return actionLog;
    }

    public void setStatus(IssueStatus status) {
        this.status = status;
        this.lastUpdated = LocalDateTime.now();
    }

    public void addAction(WorkAction action, String description) {
        if (actionCounter >= MAX_ACTIONS) {
            throw new RuntimeException("Actions shouldn't be more than " + MAX_ACTIONS);
        }

        String actionName = action + ": " + description;
        actionLog[actionCounter] = actionName;
        actions[actionCounter] = action;
        actionCounter++;

        this.lastUpdated = LocalDateTime.now();
    }

    public abstract void resolve(IssueResolution resolution);

    private boolean isReadyForResolutionBasedOnMandatoryActions() {
        boolean isMandatoryActionFound = false;
        if (this.mandatoryActions != null) {
            for (int k=0; k < mandatoryActions.length; k++){
                for (int m=0; m<actionCounter; m++){
                    if (actions[m] == mandatoryActions[k]){
                        isMandatoryActionFound = true;
                        break;
                    }
                }
                if(!isMandatoryActionFound){
                    throw new RuntimeException("Cannot resolve issue " + this.issueID + " - missing action "
                            + this.mandatoryActions[k]);
                }
                isMandatoryActionFound = false;
            }
        }
        return true;
    }

    public void issueResolve(IssueResolution resolution) {
        if (isReadyForResolutionBasedOnMandatoryActions()) {
            this.resolution = resolution;
            this.lastUpdated = LocalDateTime.now();
        }
    };
}