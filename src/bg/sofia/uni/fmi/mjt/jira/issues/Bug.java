package bg.sofia.uni.fmi.mjt.jira.issues;

import bg.sofia.uni.fmi.mjt.jira.enums.IssuePriority;
import bg.sofia.uni.fmi.mjt.jira.enums.IssueResolution;
import bg.sofia.uni.fmi.mjt.jira.enums.WorkAction;

public class Bug extends Issue {
    public Bug(IssuePriority expextedPriority, Component component, String description) {
        super(expextedPriority, component, description);
        this.mandatoryActions = new WorkAction[]{WorkAction.FIX, WorkAction.TESTS};
    }

    @Override
    public void resolve(IssueResolution resolution) {
        issueResolve(resolution);
    }
}
