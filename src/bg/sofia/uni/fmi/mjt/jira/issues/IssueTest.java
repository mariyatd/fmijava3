package bg.sofia.uni.fmi.mjt.jira.issues;

import bg.sofia.uni.fmi.mjt.jira.enums.IssuePriority;
import bg.sofia.uni.fmi.mjt.jira.enums.IssueResolution;
import bg.sofia.uni.fmi.mjt.jira.enums.IssueStatus;
import bg.sofia.uni.fmi.mjt.jira.enums.WorkAction;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IssueTest {
    private static final int WAIT_TIME = 100;

    private static Component component = new Component("grader", "GG");
    private static IssuePriority expextedPriority = IssuePriority.MAJOR;
    private static String dummyDescription = "testy";

    private static Issue bug;
    private static Issue task;
    private static Issue feature;

    @Before
    public void setUp() {
        task = new Task(expextedPriority, component, dummyDescription);
        feature = new Feature(expextedPriority, component, dummyDescription);
        bug = new Bug(expextedPriority, component, dummyDescription);
    }

    @Test
    public void testDefaultResolution() {
        IssueResolution expectedResolution = IssueResolution.UNRESOLVED;

        assertEquals(expectedResolution, task.getResolution());
        assertEquals(expectedResolution, feature.getResolution());
        assertEquals(expectedResolution, bug.getResolution());
    }

    @Test
    public void testDefaultStatus() {
        IssueStatus expectedStatus = IssueStatus.OPEN;

        assertEquals(expectedStatus, task.getStatus());
        assertEquals(expectedStatus, feature.getStatus());
        assertEquals(expectedStatus, bug.getStatus());
    }

    @Test
    public void testIDFormat() {
        String id = task.getIssueID();
        assertTrue(id.contains(String.format("%s-", task.getComponent().getShortName())));
    }

    @Test
    public void testTaskResolution() {
        IssueResolution expectedResolution = IssueResolution.WONT_FIX;
        task.resolve(expectedResolution);
        assertEquals(expectedResolution, task.getResolution());
    }

    @Test
    public void testBugResolution() {
        IssueResolution expectedResolution = IssueResolution.FIXED;
        bug.addAction(WorkAction.FIX, dummyDescription);
        bug.addAction(WorkAction.TESTS, dummyDescription);
        bug.resolve(expectedResolution);

        assertEquals(expectedResolution, bug.getResolution());
    }

    @Test(expected = RuntimeException.class)
    public void testBugResolutionFail() {
        bug.resolve(IssueResolution.FIXED);
    }

    @Test
    public void testFeatureResolution() {
        IssueResolution expectedResolution = IssueResolution.FIXED;
        feature.addAction(WorkAction.DESIGN, "design");
        feature.addAction(WorkAction.IMPLEMENTATION, "impl");
        feature.addAction(WorkAction.TESTS, "tests");
        feature.resolve(expectedResolution);

        assertEquals(expectedResolution, feature.getResolution());
    }

    @Test(expected = RuntimeException.class)
    public void testFeatureResolutionFail() {
        IssueResolution expectedResolution = IssueResolution.FIXED;
        feature.addAction(WorkAction.DESIGN, dummyDescription);
        feature.resolve(expectedResolution);
    }

    @Test(expected = RuntimeException.class)
    public void testTaskActionLogFix() {
        task.addAction(WorkAction.FIX, dummyDescription);
    }

    @Test(expected = RuntimeException.class)
    public void testTaskActionLogImpl() {
        task.addAction(WorkAction.IMPLEMENTATION, dummyDescription);
    }

    @Test(expected = RuntimeException.class)
    public void testTaskActionLogTests() {
        task.addAction(WorkAction.TESTS, dummyDescription);
    }

    @Test
    public void testResolutionModification() throws InterruptedException {
        LocalDateTime before = task.getLastModifiedOn();
        Thread.sleep(WAIT_TIME);
        task.resolve(IssueResolution.WONT_FIX);

        assertTrue(before.toLocalTime().isBefore(task.getLastModifiedOn().toLocalTime()));
    }

    @Test
    public void testStatusModification() throws InterruptedException {
        LocalDateTime before = task.getLastModifiedOn();
        Thread.sleep(WAIT_TIME);
        task.setStatus(IssueStatus.IN_PROGRESS);

        assertTrue(before.isBefore(task.getLastModifiedOn()));
    }

    @Test
    public void testCreationTime() throws InterruptedException {
        Issue task = new Task(expextedPriority, component, dummyDescription);
        LocalDateTime created = task.getCreatedOn();
        Thread.sleep(WAIT_TIME);
        task.setStatus(IssueStatus.IN_PROGRESS);

        assertTrue(created.isBefore(task.getLastModifiedOn()));
    }

    @Test
    public void testActionLogLimit() {
        addActions(task, 20);
    }

    @Test(expected = RuntimeException.class)
    public void testActionLogOverLimit() {
        addActions(task, 21);
    }

    @Test
    public void testActionLogStorage() {
        task.addAction(WorkAction.DESIGN, dummyDescription);
    }

    @Test
    public void testResearchLog() {
        assertActionLog(task, WorkAction.RESEARCH);
    }

    @Test
    public void testDesignLog() {
        assertActionLog(task, WorkAction.DESIGN);
    }

    @Test
    public void testImplementationLog() {
        assertActionLog(feature, WorkAction.IMPLEMENTATION);
    }

    @Test
    public void testTestsLog() {
        assertActionLog(feature, WorkAction.TESTS);
    }

    @Test
    public void testFixLog() {
        assertActionLog(bug, WorkAction.FIX);
    }

    private void assertActionLog(Issue issue, WorkAction action) {
        issue.addAction(action, dummyDescription);
        String expectedLog = String.format("%s: %s", action.name().toLowerCase(), dummyDescription);
        String actualLog = issue.getActionLog()[0];

        assertEquals(expectedLog, actualLog);
    }

    private void addActions(Issue issue, int count) {
        for (int i = 0; i < count; i++) {
            issue.addAction(WorkAction.RESEARCH, dummyDescription);
        }
    }
}
