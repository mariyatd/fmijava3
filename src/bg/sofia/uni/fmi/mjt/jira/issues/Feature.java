package bg.sofia.uni.fmi.mjt.jira.issues;

import bg.sofia.uni.fmi.mjt.jira.enums.IssuePriority;
import bg.sofia.uni.fmi.mjt.jira.enums.IssueResolution;
import bg.sofia.uni.fmi.mjt.jira.enums.WorkAction;



public class Feature extends Issue {
    public Feature(IssuePriority major, Component secondComponent, String description) {
        super(major, secondComponent, description);
        this.mandatoryActions = new WorkAction[]{WorkAction.DESIGN, WorkAction.IMPLEMENTATION, WorkAction.TESTS};
    }

    @Override
    public void resolve(IssueResolution resolution) {
        issueResolve(resolution);
    }
}
