package bg.sofia.uni.fmi.mjt.jira;

import bg.sofia.uni.fmi.mjt.jira.enums.IssuePriority;
import bg.sofia.uni.fmi.mjt.jira.issues.Component;
import bg.sofia.uni.fmi.mjt.jira.issues.Issue;
import bg.sofia.uni.fmi.mjt.jira.issues.Task;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class FilterTest {
    private static Component firstComponent;
    private static Component secondComponent;
    @Rule
    public ExpectedException expected = ExpectedException.none();
    private Jira jira;

    @BeforeClass
    public static void setUp() {
        firstComponent = new Component("Pesho", "pc");
        secondComponent = new Component("Ivan", "ic");
    }

    @Before
    public void initJira() {
        jira = new Jira();
    }

    @Test
    public void testFindByID() {
        Issue expected = new Task(IssuePriority.MAJOR, secondComponent, "descr");
        String id = expected.getIssueID();
        jira.addIssue(expected);

        Issue actual = jira.find(id);
        assertEquals(expected, actual);
    }

    @Test
    public void testFindByMissingID() {
        Issue expected = new Task(IssuePriority.MAJOR, secondComponent, "descr");
        String id = expected.getIssueID();

        assertNull(jira.find(id));
    }
}
