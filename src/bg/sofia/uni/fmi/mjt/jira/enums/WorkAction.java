package bg.sofia.uni.fmi.mjt.jira.enums;

public enum WorkAction {
    RESEARCH, DESIGN, IMPLEMENTATION, TESTS, FIX;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
