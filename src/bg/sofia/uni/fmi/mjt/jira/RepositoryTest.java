package bg.sofia.uni.fmi.mjt.jira;

import bg.sofia.uni.fmi.mjt.jira.enums.IssuePriority;
import bg.sofia.uni.fmi.mjt.jira.issues.Component;
import bg.sofia.uni.fmi.mjt.jira.issues.Issue;
import bg.sofia.uni.fmi.mjt.jira.issues.Task;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class RepositoryTest {
    private static Component component = new Component("SM","SM");
    private Jira jira;

    @Before
    public void setup() {
        jira = new Jira();
    }

    @Test
    public void testAdd() {
        Issue expected = getNewIssue();
        jira.addIssue(expected);

        Issue actual = jira.find(expected.getIssueID());
        assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void testMultipleAddSame() {
        Issue issue = getNewIssue();
        jira.addIssue(issue);
        jira.addIssue(issue);
    }

    @Test
    public void testJustEnoughAdds() {
        addIssues(100);
    }

    @Test(expected = RuntimeException.class)
    public void testTooManyAdds() {
        addIssues(101);
    }

    private Issue getNewIssue() {
        return new Task(IssuePriority.CRITICAL,component,  "task");
    }

    private void addIssues(int count) {
        for (int i = 0; i < count; i++) {
            Issue issue = getNewIssue();
            jira.addIssue(issue);
        }
    }
}
